<?php

namespace App\POO\Ex06;

use App\POO\Ex05\IFighter;

 abstract class Fighter implements IFighter
 {
     public $soldier_type;

     public function __construct($input)
     {
         $this->soldier_type = $input;
     }

     public function __get($input)
     {
         return $input->soldier_type;
     }
 }
