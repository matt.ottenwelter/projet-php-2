<?php

namespace App\POO\Ex00;

use App\Resources\Classes\Lannister\Lannister;

class Tyrion extends Lannister
{
    public const SIZE = 'Short';
    public const BIRTH_ANNOUNCEMENT = "My name is Tyrion\n";

    protected function announceBirth(): void
    {
        if ($this->needBirthAnnouncement) {
            echo parent::BIRTH_ANNOUNCEMENT;
            echo self::BIRTH_ANNOUNCEMENT;
        }
    }

    public function sleepWith($censored)
    {
        if (is_subclass_of($censored, 'App\Resources\Classes\Lannister\Lannister')) {
            echo "Not even if I'm drunk !\n";
        } else {
            echo "Let's do this.\n";
        }
    }
}
