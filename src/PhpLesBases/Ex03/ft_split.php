<?php

function ft_split(string $var): array
{
    $array = preg_split("/[\s,]+/", $var, 0, PREG_SPLIT_NO_EMPTY);
    sort($array);

    return $array;
}
