<?php

// Méthode 1
if ($argc > 1) {
    echo implode("\n", array_slice($argv, 1)) . "\n";
}

// Méthode 2
// foreach(array_slice($argv, 1) as $argv){

//     echo "$argv\n";

// }
