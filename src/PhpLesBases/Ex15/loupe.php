<?php

$string = file_get_contents($argv[1]);

$pattern = '/(?=").+?(?=")|(?<=">).+?(?=<)|(?<=com>).+?(?=<img)/';

// Récupérer les liens
$arrupper = [];
preg_match_all($pattern, $string, $arrlink);

// Mettre les liens en Majuscule
foreach ($arrlink[0] as $value) {
    array_push($arrupper, strtoupper($value));
}

$string = str_replace($arrlink[0], $arrupper, $string);

echo $string;
